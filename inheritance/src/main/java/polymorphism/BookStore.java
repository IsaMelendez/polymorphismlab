package polymorphism;

public class BookStore {
    public static void main(String[] args){
        Book[] myBooks = {
            new Book("Normal Book A", "Author A"),
            new ElectronicBook("Ebook A", "Author B", 4500),
            new Book("Normal Book B", "Author C"),
            new ElectronicBook("Ebook B", "Author D", 10000),
            new ElectronicBook("EBook C", "Author E", 8000)};
        
        for(int i=0; i<myBooks.length; i++){
            System.out.println(myBooks[i]);
        }

        ElectronicBook b = (ElectronicBook)myBooks[0];
        System.out.println(b.getNumberBytes());
    }
}
