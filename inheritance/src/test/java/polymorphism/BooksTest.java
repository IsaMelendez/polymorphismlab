package polymorphism;

import static org.junit.Assert.*;

import org.junit.Test;

public class BooksTest 
{
    @Test
    public void shouldCreateBook()
    {
        Book myB = new Book("abc", "auth");
        assertEquals("auth", myB.getAuthor());
        assertEquals("abc", myB.getTitle());
    }

    public void shouldCreateEBook()
    {
        ElectronicBook myB = new ElectronicBook("abc", "auth", 123);
        assertEquals("auth", myB.getAuthor());
        assertEquals("abc", myB.getTitle());
        assertEquals("123", myB.getNumberBytes());
    }
}
